document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, '');
  });


$(document).ready(function(){
    $('.tabs').tabs();
    $('.parallax').parallax();
    $('.slider').slider();
    $('.collapsible').collapsible();
    $('.modal').modal();
  });